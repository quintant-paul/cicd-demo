import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Button } from 'react-bulma-components';
 
export default function ButtonApp(){
 return  <Button color="danger" size="large" rounded outlined>Wowza!</Button>
}