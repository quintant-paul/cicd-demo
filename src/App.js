import React from 'react';
import FormApp from './component/form';

function App() {
  return (
    <div className="App">
      <section class="section">
        <div class="container">
          <h1 class="title">
            Hello DEMO
      </h1>
          <p class="subtitle">
            My website with <strong>DEMO</strong>!
      </p>
      <p>
        <FormApp></FormApp>
      </p>
        </div>
      </section>
    </div>
  );
}

export default App;
